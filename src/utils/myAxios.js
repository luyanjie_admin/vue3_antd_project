import axios from 'axios';

const instance = axios.create({
    baseURL: '', // 设置基础URL
    timeout: 8000, // 设置超时时间
});

// 请求拦截器
instance.interceptors.request.use(
    (config) => {
        // 添加跨域支持的请求头
        config.headers['Access-Control-Allow-Origin'] = '*';
        config.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE';
        config.headers['Access-Control-Allow-Headers'] = 'Content-Type';

        // 在请求发送之前进行一些处理，例如添加其他请求头等
        return config;
    },
    (error) => {
        // 处理请求错误
        return Promise.reject(error);
    }
);

// 响应拦截器
instance.interceptors.response.use(
    (response) => {
        // 在响应成功返回之前进行一些处理，例如处理响应数据等
        return response.data;
    },
    (error) => {
        // 处理响应错误
        return Promise.reject(error);
    }
);

export default instance;