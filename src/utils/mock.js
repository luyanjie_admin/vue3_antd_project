import Mock from 'mockjs'
import homeApi from '../apimock/home'

Mock.mock('/home/getData', homeApi.getStatisticalData)
