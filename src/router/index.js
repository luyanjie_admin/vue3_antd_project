import { createRouter, createWebHashHistory } from "vue-router";
const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("@/components/home.vue")
  },
  {
    path: "/message",
    name: "message",
    component: () => import("@/components/message_time.vue")
  },
  {
    path: "/hekko",
    name: "HekkoWorld",
    component: () => import("@/components/HekkoWorld.vue")
  },
  {
    path: "/content",
    name: "content",
    component: () => import("@/components/content.vue")
  },
]
const router = createRouter({
  history: createWebHashHistory(),
  routes
});
export default router;