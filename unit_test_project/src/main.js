// npm install babel-plugin-component element-plus
import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import VueCreateDM from 'vue-create-dm';
import router from "@/router"
import 'ant-design-vue/dist/antd.css';
createApp(App).use(router).use(ElementPlus).use(VueCreateDM).mount('#app')