import { createRouter, createWebHashHistory } from "vue-router";
const routes = [
          {
              path:"/",
              name:"user",
              component:()=>import("@/views/UserView.vue")
          }
        ]
const router = createRouter({
  history: createWebHashHistory(),
  routes
});
export default router;